#!/usr/bin/python3

"""
sendy

Send messages to the world via various messaging systems.
"""
import sys
import os
import yaml
import requests
from datetime import datetime

def main(argv = None):
    if argv is None:
        argv = sys.argv

    try:
        message_config_path_yaml = argv[1]
    except IndexError:
        print("Please pass the path to the message file as the first argument.")
        quit()
    if not os.path.exists(message_config_path_yaml):
        print("That message path does not exist.")
        quit()

    sendy = Sendy()
    sendy.message_config_path_yaml = message_config_path_yaml 
    sendy.run()

class Sendy:
    sendy_config = {} 
    message_config_path_yaml = None 
    message_config = {}

    def run(self):
        if not self.init_sendy_config():
            return
        if not self.init_message_config():
            return
        self.process_messages()

    def log(self, content):
        print(content)

    def yes_no_prompt(self, prompt):
        prompt = prompt + " [Yn] "
        response = input(prompt)
        if response == "" or response == "y" or response == "Y":
            return True
        return False

    def get_web_content(self, url):
        url = url + "/export/txt"
        print("Fetching {0}...".format(url))
        content = requests.get(url).text
        return content

    def get_hugo_front_matter(self, title, date, slug):
        front =  """---
        date: {0}Z
        title: "{1}"
        slug: {2} 
        tags: [ "callout"]
        ---
        """
        # Return with variables and without the indentation.
        return front.format(date.isoformat(), title, slug).replace("  ","")

    def process_messages(self):
        for medium in self.message_config:
            if not self.process_medium(medium):
                return False

    def process_medium(self, medium):
        if medium == "email":
            return self.process_email()
        elif medium == "twitter":
            return self.process_twitter()
        elif medium == "web":
            return self.process_web()
        else:
            self.log("I don't understand how to process {0}".format(medium))
            return False


    def process_web(self):
        self.log("Process web")
        # Check config.
        web_path = self.sendy_config["web"]["path"]
        content_path = web_path + "/content"
        for path in [ web_path, content_path ]:
            if not os.path.exists(path):
                self.log("Failed to find expected path: {0}.".format(path))
                return False

        # Examine our existing web content. The english slug is always the basis for the
        # path.
        date = self.message_config["web"]["date"]
        year = date.year
        target_dir = content_path + "/post/{0}/".format(year)
        if not os.path.exists(target_dir):
            os.mkdir(target_dir)

        target = {}
        target["en"] = target_dir + self.message_config["web"]["slug"]["en"] + ".en.md"
        target["es"] = target_dir + self.message_config["web"]["slug"]["en"] + ".es.md"
        
        create = False
        # Have we been created yet?
        if not os.path.exists(target["en"]):
            answer = self.yes_no_prompt("The web content {0} has not been created. Create it?".format(target["en"]))
            if answer:
                self.log("Creating...")
                create = True
            else:
                self.log("Not creating")

        else:
            answer = self.yes_no_prompt("The web content {0} exists. Update it?".format(target["en"]))
            if answer:
                self.log("Updating")
                create = True
            else:
                self.log("Not updating")

        if create:
            for lang in [ "en", "es" ]:
                title = self.message_config["web"]["title"][lang]
                slug = self.message_config["web"]["slug"][lang]
                front = self.get_hugo_front_matter(title, date, slug)
                body = self.get_web_content(self.message_config["web"]["body"][lang])
                with open(target[lang], 'w') as file:
                    print("Saving to {0}".format(target[lang]))
                    #print(front + body)
                    file.write(front + "\n" + body)

        return True
    def process_email(self):
        self.log("Process email")
        return True
    def process_twitter(self):
        self.log("Process twitter")
        return True

    def init_sendy_config(self):
        config_path = os.path.expanduser('~/.config/') 
        sendy_config_path = config_path + "sendy/"
        sendy_config_path_yaml = sendy_config_path + "config.yaml"
        paths = [ config_path, sendy_config_path ]
        for path in paths:
            if not os.path.exists(path):
                os.mkdir(path)
        if not os.path.exists(sendy_config_path_yaml):
            src = os.path.dirname(os.path.realpath(__file__)) + "/config.yaml.sample"
            with open(src, 'r') as file:
                sendy_config_content = file.read()
            with open(sendy_config_path_yaml, 'w') as file:
                file.write(sendy_config_content)
            self.log("Please edit your ~/.config/sendy/config.yaml file")
            return False
        with open(sendy_config_path_yaml, 'r') as file:
            try:
                self.sendy_config = yaml.safe_load(file.read(10000))
            except yaml.parser.ParserError as e:
                self.log("There was an error parsing your message config file.")
                print(e)
                return False

            return True

    def init_message_config(self):
        with open(self.message_config_path_yaml, 'r') as file:
            try:
                self.message_config = yaml.safe_load(file.read(10000))
            except yaml.parser.ParserError as e:
                self.log("There was an error parsing your message config file.")
                print(e)
                return False
        return True
                



if __name__ == "__main__":
    sys.exit(main())

